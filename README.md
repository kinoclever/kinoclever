# KinoClever 
Сервис, реализующий быстрый поиск фильмов

## Используемые фреймворки
 - bootstrap 4.1.1
 - bootstrap-select 1.13.1
 - django 2.0.7
 - django-rest-swagger 2.2.0
 - djangorestframework 3.8.2
 - Font Awesome 4.7.0
 - jquery 3.3.1
 - jquery-bar-rating 1.2.2
 - jquery-bootstrap-scrolling-tabs 2.4.0
 - mysql
 - rangeSlider
 - themoviedb